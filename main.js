var Poorman = require('./poorman')
var qe = require('./query-engine')
var Q = require('q')

validator_parser = qe('validator', 'base.pegjs')

weather_parser = qe('weather', 'weather.pegjs')
weather_parser.setSolutionResolver(function () {
	var d = Q.defer();
	//var wunderbar = require('wunderbar');
	//var weather   = new wunderbar('6173f9953c64acd9');

	var request = require("request");
	location = this.getWordsForOnlyTag('noun.location').join(" ")
	console.log('ahoy!', location)
	request("http://api.wunderground.com/api/6173f9953c64acd9/conditions/q/" +location+".json", function(error, response, body) {
	  res = JSON.parse(body);
	  //console.log(res.current_observation);
	  weather = res.current_observation
	  //console.log(weather)
	  solution = 'Weather in ' + weather.display_location.full + ' is ' + weather.weather + ' and temperature is ' + weather.temperature_string
	  d.resolve(solution);
	});

	return d.promise
	//return 'wants weather information for ' + this.getWordsForTag('noun.location').join()
})

poorman =  new Poorman()
poorman.addQueryEngine(weather_parser)
poorman.process("what is the weather in jakarta tomorrow", function (result) {
	solutions = Array()
	if (result instanceof Array) {
		result.forEach(function(qe) {
			solutions.push(qe.getSolutions())
		})
	} else
		solutions.push(result)

	//console.log('yoo',solutions)
	
	Q.all(solutions).then(function (v) {console.log(v.join(""))});
})