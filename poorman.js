var PEG = require('pegjs')
var natural = require('natural')
var wordnet = new natural.WordNet()
var Q = require('q')
//var codein = require('node-codein')

var lexnames = [
'adj.all', 
'adj.pert', 
'adv.all', 
'noun.Tops', 
'noun.act', 
'noun.animal', 
'noun.artifact', 
'noun.attribute', 
'noun.body', 
'noun.cognition', 
'noun.communication', 
'noun.event', 
'noun.feeling', 
'noun.food', 
'noun.group', 
'noun.location', 
'noun.motive', 
'noun.object', 
'noun.person', 
'noun.phenomenon', 
'noun.plant', 
'noun.possession', 
'noun.process', 
'noun.quantity', 
'noun.relation', 
'noun.shape', 
'noun.state', 
'noun.substance', 
'noun.time', 
'verb.body', 
'verb.change', 
'verb.cognition', 
'verb.communication', 
'verb.competition', 
'verb.consumption', 
'verb.contact', 
'verb.creation', 
'verb.emotion', 
'verb.motion', 
'verb.perception', 
'verb.possession', 
'verb.social', 
'verb.stative', 
'verb.weather', 
'adj.ppl'
]


/*
http://tech.karbassi.com/2009/12/17/pure-javascript-flatten-array/
*/
function flatten(suspect){
    var flat = [];
    if (suspect instanceof Array) {
    	for (var i = 0, l = suspect.length; i < l; i++){
	        var type = Object.prototype.toString.call(suspect[i]).split(' ').pop().split(']').shift().toLowerCase();
	        if (type) { flat = flat.concat(/^(array|collection|arguments|object)$/.test(type) ? flatten(suspect[i]) : suspect[i]); }
	    } 
	    return flat
	} else
        return suspect;
}

function findGrammarMatch(parser, expr, reduceFunction) {
	if (typeof was_starting_at === "undefined")
		was_starting_at = 0;
	if (typeof tokens_length === "undefined")
		tokens_length = expr.length;

	try {

		var parse_result = parser.parse(expr);
		var res = parse_result!==false?flatten(parse_result):false
		//console.log('match: ', flatten(parse_result));
		return res;
	} catch (e) {
		//console.log(e)
		if (e.found) {
			//console.log('found match: ', expr.substr(0, e.column-1));
			//console.log('about to parse: ', expr.substr(0, e.column-1))
			var partial_res = findGrammarMatch(parser, expr.substr(0, e.column-1), reduceFunction);
			
			var next_res = false;
			if (partial_res && e.column<expr.length) 
				next_res = findGrammarMatch(parser, expr.substr(e.column), reduceFunction);

			if (next_res) {
				var flatten_result = []
				if (partial_res instanceof Array)
					flatten_result = partial_res
				else
					flatten_result.push(partial_res)

				if (next_res instanceof Array)
					for (fi in next_res)
						flatten_result.push(next_res[fi])
				else
					flatten_result.push(next_res)

				return flatten_result
			} else
				return partial_res;
		} else {
			console.log(e.name)
			if (expr.length>1) {
				//console.log('Skip 1 tokens');
				return findGrammarMatch(parser, reduceFunction(expr), reduceFunction)
			} else {
				//console.log('eof');
				return false;
			}
		}
	}
}

function reduceTokenLeft(expr) {
	tokens = /\s*[^\s]+(.*)/.exec(expr)
	if (tokens[1])
		return tokens[1]
	else
		return false
}

function reduceTokenRight(expr) {
	tokens = /(.*?)\s+[^\s]+\s*$/.exec(expr)
	if (tokens[1])
		return tokens[1]
	else
		return false
}



function findGrammarsMatch(qes, expr) {
	var result = []
	qes.forEach(function(qe) {
		//console.log(qe.parser())
		var res1 = findGrammarMatch(qe.parser(), expr, reduceTokenLeft);
		var res2 = findGrammarMatch(qe.parser(), expr, reduceTokenRight);
		var combined = []
		if (res1)
			combined.push(res1)
		if (res2)
			combined.push(res2)
		console.log('fgm', res1, res2, combined)
		if (combined.length>0) {
			qe.setResult(flatten(combined));
			result.push(qe)
		}
	})

	return result.length?result:false;
}

function getFlatLexnames(word) {
	var deferred = Q.defer();
	wordnet.lookup(word, function(results) {
		var lexcat = []
	    results.forEach(function(result) {
	        lexname = lexnames[result.lexFilenum];
	        //console.log(lexname, lexcat.indexOf(lexname))
	        if (lexcat.indexOf(lexname)==-1)
	        	lexcat.push(lexname)
	        //console.log(lexcat)
	    });
	    lexcats = lexcat.join(",")
	    if (lexcats)
	    	result = word + '/' + lexcats
	    else
	    	result = '';
	    deferred.resolve(result)
	})
	return deferred.promise;
}

function generateLexicalExpression(text) {
	var words = text.split(/\s+/)
	var result = [];
	var queue = []

	for(i in words)
		queue.push(Q.fcall(getFlatLexnames, words[i]))

	return Q.all(queue)
}

function createParserFromFile(filename) {
	var fs = require('fs'); // for loading files

	// Read file contents
	var data = fs.readFileSync(filename, 'utf-8');
	// Show the PEG grammar file
	//console.log(data);
	// Create my parser
	var parser = PEG.buildParser(data)

	return parser;
}

function Poorman () {
	if(false === (this instanceof Poorman)) {
        p = new Poorman();
        return p;
    } 
    this._PARSERS = []
}

Poorman.prototype.addQueryEngine = function(qe) {
	this._PARSERS.push(qe)
}

Poorman.prototype.process = function (text, callback) {
	var self = this
	Q.fcall(generateLexicalExpression, text).then(function (res) {
		var tagged = res.join(" ")
		console.log('done', tagged)
		var result = findGrammarsMatch(self._PARSERS, tagged)
		self._result = result
		//console.log('final result', result)
		if (callback) callback(result)
	})
}

module.exports = Poorman

