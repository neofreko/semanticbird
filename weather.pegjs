start
= left:weather blank right:location+ {return [left, right]}
expr 
    = & blank* left:(primitive / compound)+ blank*

compounds
    = left:compound+ {return left}

compound
	= left:primitive blank right:primitive {return left+right}

primitive
    = word:word left:lexhint right:("," lexhint)* {return {word: word, lexhint: left + String(right).replace(/,+/g, ',') }}

word "word"
    =  str:(!"/" c:. {return c})+ "/" {return str.join("")}

lexhints
    = hint:((lexhint ',')* lexhint) {return ((hint instanceof Array)?hint.join(""):hint).replace(/,+/g, ',')}

lexhint
    = str:('adj.pert'
	/ 'adj.all'
	/ 'adv.all'
	/ 'noun.Tops'
	/ 'noun.act'
	/ 'noun.animal'
	/ 'noun.artifact'
	/ 'noun.attribute'
	/ 'noun.body'
	/ 'noun.cognition'
	/ 'noun.communication'
	/ 'noun.event'
	/ 'noun.feeling'
	/ 'noun.food'
	/ 'noun.group'
	/ 'noun.location'
	/ 'noun.motive'
	/ 'noun.object'
	/ 'noun.person'
	/ 'noun.phenomenon'
	/ 'noun.plant'
	/ 'noun.possession'
	/ 'noun.process'
	/ 'noun.quantity'
	/ 'noun.relation'
	/ 'noun.shape'
	/ 'noun.state'
	/ 'noun.substance'
	/ 'noun.time'
	/ 'verb.body'
	/ 'verb.change'
	/ 'verb.cognition'
	/ 'verb.communication'
	/ 'verb.competition'
	/ 'verb.consumption'
	/ 'verb.contact'
	/ 'verb.creation'
	/ 'verb.emotion'
	/ 'verb.motion'
	/ 'verb.perception'
	/ 'verb.possession'
	/ 'verb.social'
	/ 'verb.stative'
	/ 'verb.weather'
	/ 'adj.ppl') { return str }

lexhint_location
 = str:"noun.location" {return str}

location
 = word:word hint:lexhints & { return /noun\.location/.test(hint)} {return {word: word, lexhint: hint } }

lexhint_weather
 = str:"noun.phenomenon" {return str}

weather
= word:word str:(! lexhint_weather c:. )* hint:lexhint_weather  {return {word: word, lexhint: hint } }

blank "blank"
	= " "+ {return " "}