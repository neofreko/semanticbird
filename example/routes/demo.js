var Poorman = require('../../poorman')
var qe = require('../../query-engine')
var Q = require('q')

//validator_parser = qe('validator', '../../base.pegjs')

weather_parser = qe('weather', '../weather.pegjs')
weather_parser.setSolutionResolver(function () {
	console.log('ahoy')
	var d = Q.defer();
	//var wunderbar = require('wunderbar');
	//var weather   = new wunderbar('6173f9953c64acd9');

	var request = require("request");
	console.log('here')
	location = this.getWordsForOnlyTag('noun.location').join()
	console.log('ahoy!', this.getWordsForTag('noun.location').join())
	request("http://api.wunderground.com/api/6173f9953c64acd9/conditions/q/" +location+".json", function(error, response, body) {
		res = JSON.parse(body);
		//console.log(res.current_observation);
		weather = res.current_observation
		if (weather) {
			//console.log(weather)
			solution = 'Weather in ' + weather.display_location.full + ' is ' + weather.weather + ' and temperature is ' + weather.temperature_string
		} else
			solution = 'Cannot resolve location. Please be more specific'
	  	d.resolve(solution);
	});

	return d.promise
	//return 'wants weather information for ' + this.getWordsForTag('noun.location').join()
})


poorman =  new Poorman()
poorman.addQueryEngine(weather_parser)

exports.query = function(req, res){
	console.log(req.query.query)

	poorman.process(req.query.query, function (result) {
		solutions = Array()
		if (result instanceof Array) {
			result.forEach(function(qe) {
				//console.log('foo',qe)
				solutions.push(qe.getSolutions())
			})
			//console.log(solutions)
			//return solutions
		} else
			solutions.push(result)
		//console.log(solutions)
		
		Q.all(solutions).then(function (v) {
			var str = v.join("")
			var result = {
				success: str != 'false',
				message: str
			}
			console.log(v.join(""));
			res.send(JSON.stringify(result))
		});
	})
};