/**
QueryEngine
*/

var PEG = require('pegjs')

function createParserFromFile(filename) {
	var fs = require('fs'); // for loading files

	// Read file contents
	var data = fs.readFileSync(filename, 'utf-8');
	// Show the PEG grammar file
	//console.log(data);
	// Create my parser
	var parser = PEG.buildParser(data)

	return parser;
}

function QE (name, filename) {
	if(false === (this instanceof QE)) {
		//console.log(name)
        q = new QE();
        if (filename) 
        	q.createFromFile(filename)
    	q.name = name
        return q;
    } else {
    	if (filename) 
        	this.createFromFile(filename)
    	this.name = name
    }
}

QE.prototype.createFromFile = function (filename) {
	this._parser = createParserFromFile(filename)
}

QE.prototype.setResult = function (result) {
	this._result = result
	//console.log(result)
}

QE.prototype.getWordsForTag = function (tag) {
	//console.log('tag',tag, this._result )
	var re = new RegExp(tag.replace(/\./g, '\.'));
	if (this._result instanceof Array) {
		res = this._result.filter(function(item) {
			//console.log('re',item.lexhint,re.test(item.lexhint)); 
			return re.test(item.lexhint)
		})
		//console.log('word4tag', res)
		if (res)
			return res.map(function(item) {return item.word})
		else
			return false
	} else
		return false
}

QE.prototype.getWordsForOnlyTag = function (tag) {
	//console.log('tag',tag, this._result )
	if (this._result instanceof Array) {
		res = this._result.filter(function(item) {
			//console.log('re',item.lexhint,re.test(item.lexhint)); 
			return item.lexhint==tag
		})
		//console.log('word4tag', res)
		if (res)
			return res.map(function(item) {return item.word})
		else
			return false
	} else
		return false
}

QE.prototype.setSolutionResolver = function (func) {
	this._resolver = func
}

QE.prototype.getSolutions = function () {
	//console.log('want solution')
	return this._resolver.call(this)
}

QE.prototype.parser = function () {return this._parser}
QE.prototype.name = function () {return this._name}

module.exports = QE