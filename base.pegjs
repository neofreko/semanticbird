start
	= expr
expr 
    = & blank* left:(primitive / compound)+ blank* {return left}

compounds
    = left:compound+ {return left}

compound
	= left:primitive blank right:primitive {return [left, right]}

primitive
    = word:word left:lexhint right:("," lexhint)* {return {word: word, lexhint: left + String(right).replace(/,+/g, ',') }}

word "word"
    =  str:(!"/" c:. {return c})+ "/" {return str.join("")}

lexhint
    = str:('adj.pert'
	/ 'adj.all'
	/ 'adv.all'
	/ 'noun.Tops'
	/ 'noun.act'
	/ 'noun.animal'
	/ 'noun.artifact'
	/ 'noun.attribute'
	/ 'noun.body'
	/ 'noun.cognition'
	/ 'noun.communication'
	/ 'noun.event'
	/ 'noun.feeling'
	/ 'noun.food'
	/ 'noun.group'
	/ 'noun.location'
	/ 'noun.motive'
	/ 'noun.object'
	/ 'noun.person'
	/ 'noun.phenomenon'
	/ 'noun.plant'
	/ 'noun.possession'
	/ 'noun.process'
	/ 'noun.quantity'
	/ 'noun.relation'
	/ 'noun.shape'
	/ 'noun.state'
	/ 'noun.substance'
	/ 'noun.time'
	/ 'verb.body'
	/ 'verb.change'
	/ 'verb.cognition'
	/ 'verb.communication'
	/ 'verb.competition'
	/ 'verb.consumption'
	/ 'verb.contact'
	/ 'verb.creation'
	/ 'verb.emotion'
	/ 'verb.motion'
	/ 'verb.perception'
	/ 'verb.possession'
	/ 'verb.social'
	/ 'verb.stative'
	/ 'verb.weather'
	/ 'adj.ppl') { return str }

blank "blank"
	= str:" "+ {return " "}